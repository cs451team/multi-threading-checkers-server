import java.io.DataOutputStream;
import java.io.EOFException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

public class gameThread extends Thread{

		static int status=0;
		static Move move=new Move();
		static Move nmove=new Move();
		private  ObjectInputStream is = null;
		private  ObjectOutputStream os = null;
		private Socket clientSocket = null;
		final gameThread[] threads;
		private  int maxClientsCount;
		int playerNo = -1;
	
	public gameThread(Socket clientSocket, gameThread[] threads)
	{
		this.clientSocket = clientSocket;
		this.threads = threads;
		maxClientsCount = threads.length;
		if(status==0)
			playerNo = 0;
		else if(status==1&&threads[0]==null)
			playerNo = 0;
		else {
			
			playerNo = 1;}
		
		status=status+1;
	}
	
	
	 public void run() {
		 int maxClientsCount = this.maxClientsCount;
		    gameThread[] threads = this.threads;
		  
		    try {
		      /*
		       * Create input and output streams for this client.
		       */
		    	 
		    	 is = new ObjectInputStream(clientSocket.getInputStream());
		    	 os = new ObjectOutputStream(clientSocket.getOutputStream());
		     
		      	if(playerNo==0)
		      	{
		      		System.out.println("first client");
		      		threads[0].os.writeObject("1");
		      		if(status==2&&threads[1]!=null)
		      		{
		      			String status1=status+"";
		      			threads[0].os.writeObject(status1);
		      		}
		      		
		      	} 
		      	else if(playerNo==1)
		      	{
		      		System.out.println("second client");
		      		threads[1].os.writeObject("2");
		      		
		      		
		      		String status1=status+"";
		      			threads[0].os.writeObject(status1);
		      			
		     	}
		      	
		      	
		      
		       /*{
		      /* Start exchanging moves */
		 if(threads.length>1){
		      	while (true) 
		      {
		    	  	try
		    	  	{
		    	  		move = (Move) is.readObject();
		    	  	
		    	  		if (move==null) {
		    	  				break;
		    	  		}
		    	  		System.out.println("Server recieved the move from player"+this.playerNo);
		    	  		move.printMove();
		    	  		
		    	  		
		    	  		nmove.setTo(move);
		    	  	
			    	  	if (this.playerNo==0)
			    	  	{
			    	  		move.myturn = false;
			    	  		nmove.myturn = true;
			    	  
			    	  	} 
			    	  	else
			    	  	{
			    	  		move.myturn = true;
			    	  		nmove.myturn = false;
			    	  		
			    	  	}
		    	  	}
		    	  	
		    	  		catch (EOFException e) {
		    	  			 break;
					    		  
					    	  							}
		    	  		
		    	 
		       //Synchronizing both clients thread
		
		    	  	synchronized (this) {
		          for (int i = 0; i < maxClientsCount; i++) {
		        	  if (threads[i] != null && threads[i].playerNo != -1 ) {
		        		  		if(i==0)
		        		  			{System.out.println("Sending move to player1: \n");
		        		  			move.printMove();
		        		  			threads[i].os.reset();
		        		  			threads[i].os.writeObject(move);
		        		  			}
		        		  		else if(i==1) {
		        		  			System.out.println("Sending move to player2: \n");
		        		  			nmove.printMove();
		        		  			threads[i].os.reset();
		        		  			threads[i].os.writeObject(nmove);
		        		  		}
		        	  										}
		          			}
		    	  					}
		      			}
		  
		      /*   if game ends
		       * Clean up. Set the current thread variable to null so that a new client
		      	
		       */
		     synchronized (this) {
		      for (int i = 0; i < maxClientsCount; i++) {
		    	  if (threads[i] == this) {
		    		  status--; 
		    		  threads[i] = null;
		    		  //Decreasing the status of clients connected to server
		    	  							}
		      											}
		     					}
		      /*
		       * Close the output stream, close the input stream, close the socket.
		       */
		     System.out.println("closing thread");
		     is.close();
		     os.close();
		     clientSocket.close();
		     
		 } //Catch any error
		    } catch (Exception e) {
		    	
				System.out.println("Error in server");
				 synchronized (this) {
				      for (int i = 0; i < maxClientsCount; i++) {
				    	  if (threads[i] == this) {
				    		 
					    			status--; 
				    		  threads[i] = null;
				    		  
				    	  							}
				      											}
				      try {
						clientSocket.close();
					} catch (IOException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				     					}
				  System.out.println("closing thread");
			}
		  }

		}
