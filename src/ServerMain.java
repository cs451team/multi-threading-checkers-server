import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;

public class ServerMain {
	
	static gameThread[] threads = new gameThread[2];
	
	public static void main(String args[]) throws ClassNotFoundException
	{
		// The client socket and server socket
		ServerSocket serverSocket = null;
		
		Socket clientSocket = null;

		// This chat server can accept up to maxClientsCount clients' connections.
		final int maxClientsCount = 2;
		
		try 
		{
			//creating server socket with port number
			serverSocket = new ServerSocket(1900);
			System.out.println("Server connected");
			
			while (true) 
			{
				clientSocket = serverSocket.accept();
			
				System.out.println("Client connected");
				
				//Allocate thread to 2 game clients
				int i = 0;
				for (i = 0; i < maxClientsCount; i++) 
				{
					if (threads[i] == null) 
					{
						(threads[i] = new gameThread(clientSocket, threads)).start();
						break;
					}
				}
				if (i == maxClientsCount)
				{
					System.out.println("Serveris busy for clients");
					ObjectOutputStream os1 = new ObjectOutputStream(clientSocket.getOutputStream());
					os1.writeObject("Server too busy. Try later.");
					os1.close();
				}
			}
		} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
		}
	}
}


